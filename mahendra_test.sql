-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 12, 2020 at 01:37 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahendra_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`) VALUES
(1, 'Test', '2020-03-11 11:48:05'),
(2, 'Test 1', '2020-03-11 11:48:05');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `comment` text,
  `created_by` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `event_id`, `user_id`, `parent_id`, `comment`, `created_by`) VALUES
(1, 1, 1, 0, 'Test comment', '2020-03-11 12:05:32'),
(2, 1, 1, 1, 'Test comment 12121', '2020-03-11 12:05:51'),
(3, 1, 1, 1, 'Test comment 12121', '2020-03-11 12:25:04'),
(4, 1, 1, 1, 'Test comment 12121', '2020-03-11 12:32:46');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `location_id`, `date`, `title`, `description`, `created_at`) VALUES
(1, 1, '2020-03-11 00:00:00', 'Test Event 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-03-11 07:00:59'),
(2, 2, '2020-03-12 00:00:00', 'Test Event 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2020-03-11 07:00:59'),
(3, 1, '2020-03-11 00:00:00', 'Test sdf', 'fasdfasdfsadf', '2020-03-11 10:50:29'),
(4, 1, '2020-03-11 00:00:00', 'asdfsdf', 'Test afdsafsafasdf', '2020-03-11 10:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `event_category`
--

CREATE TABLE `event_category` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_category`
--

INSERT INTO `event_category` (`id`, `event_id`, `category_id`, `created_at`) VALUES
(5, 4, 2, '2020-03-11 11:17:16'),
(6, 4, 1, '2020-03-11 11:17:16');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `location_name` varchar(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `location_name`, `created_at`) VALUES
(1, 'Indore', '2020-03-11 07:01:14'),
(2, 'Bhopa', '2020-03-11 07:01:14');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0430c0a3288618cf9f4f5f32a9f8b40186b9e57dc2d9aea74e4902614c95e10f11c933a6503d9e36', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:03:57', '2020-02-19 08:03:57', '2021-02-19 13:33:57'),
('0cca92a081131fac4b825b34517b1b44d3748188dc6d139863166dd87ff1cb8990a5c73b5cce67da', 2, 1, '1', '[\"place-orders\",\"manage-order\"]', 0, '2020-02-19 08:56:13', '2020-02-19 08:56:13', '2021-02-19 14:26:13'),
('0ee40acabc36575beb6daa18e116f7255d5f846d39737eb9750a9dae33f80cc7fa0920fa46e752c2', 2, 1, '1', '[\"place-orders\",\"manage-order\"]', 0, '2020-02-19 08:55:29', '2020-02-19 08:55:29', '2021-02-19 14:25:29'),
('10515c6a1df1c8667fb68dd51435bb5a78694e9cd16f2814781107a3b8d11fed30069f300942c4f5', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:49:12', '2020-03-12 01:49:12', '2021-03-12 07:19:12'),
('11a6fd04640f70737e7a456c1337402ea1e7c656d5eba4220f7121f9d7a06f23d2746c6683238c5e', 1, 1, '1', '[\"manage-order\"]', 0, '2020-03-11 01:19:04', '2020-03-11 01:19:04', '2021-03-11 06:49:04'),
('1526a8cb2b16ecbca65cb813fff99b8f73e42248ea9397b0ae431b7419cebe71de66abad51243bed', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:50:45', '2020-03-12 01:50:45', '2021-03-12 07:20:45'),
('2a6e608f41c27e78687790140f8fe19a1e68a4ef3e1c7b8aac3e6238908103ab941039fc93a04c1e', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:00:19', '2020-02-19 08:00:19', '2021-02-19 13:30:19'),
('4b43454da3a9dd6cac7972a9f8051f45307a9e904f05cd8cd0727ea837beda5b2cdf7149b01c37b2', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:17:16', '2020-03-12 01:17:16', '2021-03-12 06:47:16'),
('4e4df96d6cca43bd10b172d2a6b907d0bebc77a67d337f82c6460f54e91563bed8e94d6d0bb7241e', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:04:46', '2020-03-12 01:04:46', '2021-03-12 06:34:46'),
('51349b7f691b75ac69ea37252e9ac5c7fe3429283487805c45e5fb039aea4bc1458af202ef8b5f3b', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 00:28:52', '2020-03-12 00:28:52', '2021-03-12 05:58:52'),
('53ea359fa31421952e4193d2c49cddcf7b225088443eb16a53954c0f14a3e9cb9b35393fba3d65d7', 1, 1, '1', '[\"place-orders\",\"manage-order\"]', 0, '2020-03-11 01:13:00', '2020-03-11 01:13:00', '2021-03-11 06:43:00'),
('56ac21eee2ed27b6b80a8f4b1fdaec0da19d0e8e9d2dcc1a98768bf369890a427e8f8ddd09498853', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:15:54', '2020-03-12 01:15:54', '2021-03-12 06:45:54'),
('5fc6ffa3dd07e5bbf365d4d3839ca446295070f9c1e3762a40d5361770848f63932bf632215d4c2e', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:47:26', '2020-03-12 01:47:26', '2021-03-12 07:17:26'),
('6421816a4a790593d6a5f26db47cd6b0b5480a167b3d506f5348761bc00af617675bf4af9c2e7d65', 2, 1, NULL, '[]', 0, '2020-02-19 07:59:45', '2020-02-19 07:59:45', '2021-02-19 13:29:45'),
('64cd5b57257e2ead765021638a087b4d91f5a33ce9d1cbd59bc550f3061569b9188e71aa5330faa9', 1, 1, '1', '[\"admin\"]', 0, '2020-03-11 01:20:06', '2020-03-11 01:20:06', '2021-03-11 06:50:06'),
('73792375c49955a7960bbfd7e609c241414603507da896f5f1b7d9446dfceb2e770fe26b181a3e04', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:11:40', '2020-03-12 01:11:40', '2021-03-12 06:41:40'),
('77469dc0555d0d318d89db0969d958b31d4ba9b08776d86ce61cde1e953f5dc043f9cffb766a9c70', 1, 1, '1', '[\"admin\"]', 0, '2020-03-11 01:37:36', '2020-03-11 01:37:36', '2021-03-11 07:07:36'),
('7870027f58da833a4bba4c2ffc2ddd54b42a9bddecf9e87a435f22a714bd428a075dd2d709aeaa18', 2, 1, '2', '[\"user\"]', 0, '2020-03-12 01:43:05', '2020-03-12 01:43:05', '2021-03-12 07:13:05'),
('7ce9e84aec3f327977f40a5ce8d7887e6ea0222ffcd3bbf8a2e429c10b91dd5398fee73df32a90f1', 2, 1, '2', '[]', 0, '2020-02-19 08:43:48', '2020-02-19 08:43:48', '2021-02-19 14:13:48'),
('8226a2e8f77ab924e3b8dee8751e9c0014487e329a134fa84f0b186d731667928ef636e39700544f', 1, 1, '1', '[\"admin\"]', 0, '2020-03-11 01:25:01', '2020-03-11 01:25:01', '2021-03-11 06:55:01'),
('88541eb328d5ac6644d34a706b2e45df6841008213416d6f746ba46c36121d9b337d4247accf1824', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:51:42', '2020-03-12 01:51:42', '2021-03-12 07:21:42'),
('88b4a4f6725f0ad28edb5dce9bf48993bed70f07520553358dea448a6062f7c85f738acb2ea4ea32', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:19:31', '2020-02-19 08:19:31', '2021-02-19 13:49:31'),
('8b5de4b67eb568c24f53398da2b2e6cb911b2552b529236664bdb84c8cc2c31be73301a6e4750f4a', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:04:08', '2020-02-19 08:04:08', '2021-02-19 13:34:08'),
('90872a8bd05d5a990cebb72a251350308153565da805ccbfa6961feb755f365d5ee48d2d099dbaf6', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:04:29', '2020-02-19 08:04:29', '2021-02-19 13:34:29'),
('953fe3473053e6265854e80c2867f7b69929b23b49fb70b66a70fa67d0860e69c1d699322a9471f5', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 02:09:02', '2020-03-12 02:09:02', '2021-03-12 07:39:02'),
('9d89aeb4d9ad39d21ea16795d48ab03d173b723a9a933399f9d272cfece123df2cffadeec800ed62', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:45:13', '2020-03-12 01:45:13', '2021-03-12 07:15:13'),
('a8bfca318a4aa5eae7b5a9beb6f9867037645184d1616cb98c5ab9e83f0598f60ef67dd3ab353681', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:50:17', '2020-03-12 01:50:17', '2021-03-12 07:20:17'),
('ae4b013f5fe53d0b86d5ebe4d5b82182624dc72ed13ee432dfae84d941027d6405a519c8eb9ed5eb', 1, 1, '1', '[\"admin\"]', 0, '2020-03-11 01:20:18', '2020-03-11 01:20:18', '2021-03-11 06:50:18'),
('c572f5297f81d5c9baad6da0f2a73c6a09aef8677c8da0424f3cda9c2e2ea01792163d8a10a3958f', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:52:18', '2020-02-19 08:52:18', '2021-02-19 14:22:18'),
('c69d0ff4f06d42d7bf9c79cc53821bc4e72e32e3cb6722ea559f72b8d53339e922ee2a4748e5062d', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:44:08', '2020-03-12 01:44:08', '2021-03-12 07:14:08'),
('ca8a97a78d018784a920576506d561ef3097393be17fe0a3f5bc792b635250061615739a642ae45d', 2, 1, '1', '[\"place-orders\"]', 0, '2020-02-19 08:00:55', '2020-02-19 08:00:55', '2021-02-19 13:30:55'),
('d1436d915ff9107d81b9c365e14777e2558d4cab33f79a9b1c9131f17c0c1d5021e291d13bd17e86', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:04:18', '2020-03-12 01:04:18', '2021-03-12 06:34:18'),
('d6029f7102fd8c43d78d879b5db4b156d26a3322759cb67bba54ee5aacec14772152c1e4bb41e18b', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:02:29', '2020-03-12 01:02:29', '2021-03-12 06:32:29'),
('d93b8b583698b90c079257da128ba8d665591b73499e39b94ca2702f26d5661c4edfb5ac598c98c9', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:10:08', '2020-03-12 01:10:08', '2021-03-12 06:40:08'),
('dd7cdb311a7a3edf1cc6c74939bd4f98434de684cbc93164f99d9492d481f86560a22f84e70b99a8', 2, 1, NULL, '[]', 0, '2020-02-19 07:59:39', '2020-02-19 07:59:39', '2021-02-19 13:29:39'),
('e36c8ceff3efc430bc40a7810b5db73bedf977aa723c9ae2786bcaab5863ee08fa0ef747cc2cf2a4', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:10:35', '2020-03-12 01:10:35', '2021-03-12 06:40:35'),
('e97184b3bbe5fe4c2d2ebdc21647fd90f177cbb9088b2b2c55cbd0ff33a8f0f495ae1bffc4cd8123', 1, 1, '1', '[\"admin\"]', 0, '2020-03-12 01:03:42', '2020-03-12 01:03:42', '2021-03-12 06:33:42');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'BVm66OCm3FI17HXyGjk9vy9Yj7LMN7Yw8GyL8eQ2', 'http://localhost', 1, 0, 0, '2020-02-19 06:58:06', '2020-02-19 06:58:06'),
(2, NULL, 'Laravel Password Grant Client', 'yaWa1NtfZIM0RSt31xxxai7T5xZ0I4In1Io7Kb2N', 'http://localhost', 0, 1, 0, '2020-02-19 06:58:06', '2020-02-19 06:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-02-19 06:58:06', '2020-02-19 06:58:06');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_role` enum('admin','user') NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_role`, `name`, `email`, `password`, `created_at`) VALUES
(1, 'admin', 'Mahendra', 'admin@gmail.com', '$2y$10$aKEnZsETyyf/8hOiaeBRNuL8dTf5BXyjnhP.QxmBhcAGd6BQeAo4u', '2020-03-11 06:31:51'),
(2, 'user', 'Mahendra Gurjar', 'mahendra@gmail.com', '$2y$10$aKEnZsETyyf/8hOiaeBRNuL8dTf5BXyjnhP.QxmBhcAGd6BQeAo4u', '2020-03-11 06:31:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`),
  ADD KEY `location_id` (`location_id`);

--
-- Indexes for table `event_category`
--
ALTER TABLE `event_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `event_id` (`event_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `event_category`
--
ALTER TABLE `event_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`location_id`) REFERENCES `locations` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_category`
--
ALTER TABLE `event_category`
  ADD CONSTRAINT `event_category_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
