<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    protected $guarded = ['id'];
    protected $table = 'event_category';
    public $timestamps = false;

}
