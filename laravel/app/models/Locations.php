<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Locations extends Model
{
    protected $guarded = ['id'];
    protected $table = 'locations';
    public $timestamps = false;

}
