<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Models\Events;
use App\Models\EventCategory;
use App\Models\Comments;
class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $events = Events::get();
        return response()->json(['message'=>'Events List','status'=>1,'data'=>$events]);
    
    }

    public function add(Request $request){
        $rules['title'] = 'required';
        $rules['date'] = 'required';
        $rules['description'] = 'required';
        $rules['location_id'] = 'required';
        $rules['categories.*']= 'required';
        $rules['categories']  = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $event = new Events();
            $event->location_id    = request('location_id');
            $event->date           = date('Y-m-d',strtotime(request('date')));
            $event->title          = request('title');
            $event->description    = request('description');
            $event->save();
            $eventId = $event->id;
            if($eventId){
                $categoryIds = request('categories');
                if(!empty($categoryIds)){
                    foreach($categoryIds as $categoryId){
                        $eventCategory = new EventCategory();
                        $eventCategory->category_id = $categoryId;
                        $eventCategory->event_id    = $eventId;
                        $eventCategory->save();
                    }
                }
                $message = 'Event added successfully';
                $status = 1;
            }else{
                $message = 'Event not added';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

    public function delete(Request $request){
        $rules['event_id'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $eventId = request('event_id');
            // Delete event categorys
            EventCategory::where('event_id',$eventId)->delete();

            // Delete event
            $res = Events::where('id',$eventId)->delete();
            if($res){
                $message = 'Event deleted successfully';
                $status = 1;
            }else{
                $message = 'Event not deleted';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

    public function update(Request $request){
        $rules['title'] = 'required';
        $rules['date'] = 'required';
        $rules['description'] = 'required';
        $rules['location_id'] = 'required';
        $rules['categories.*']= 'required';
        $rules['categories']  = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $eventId = request('event_id');

            $event = Events::where(['id'=>$eventId])->first();
            if(!empty($event)){
                $eventDate = request('date');
                $event->location_id    = request('location_id');
                $event->date           = date('Y-m-d',strtotime($eventDate));
                $event->title          = request('title');
                $event->description    = request('description');
                $event->save();
                
                if($eventId){
                    // Delete old event categorys
                    EventCategory::where('event_id',$eventId)->delete();

                    // Add new event categorys
                    $categoryIds = request('categories');
                    if(!empty($categoryIds)){
                        foreach($categoryIds as $categoryId){
                            $eventCategory = new EventCategory();
                            $eventCategory->category_id = $categoryId;
                            $eventCategory->event_id    = $eventId;
                            $eventCategory->save();
                        }
                    }
                }
                $message = 'Event updated successfully';
                $status = 1;
            }else{
                $message = 'Event not updated';
                $status = 0;
            }
            
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

    public function enventDetails(Request $request,$event_id){
        $events = Events::with('Comments')->where(['id'=>$event_id])->first();
        return response()->json(['message'=>'Events Details','status'=>1,'data'=>$events]);
    }

    public function addComment(Request $request){
        $user = Auth::user();
        if(@$user->id){
            $rules['comment']  = 'required';
            $rules['event_id'] = 'required';
            $rules['parent_id'] = 'required';
            $messages = [
                'required' => 'Field is required'
            ];
            $validate = Validator::make($request->all(), $rules, $messages);
            if($validate->fails()) {
                $messages = $validate->messages();
                foreach ($messages->all(':message') as $message)
                {
                    $m = $message;
                }
                return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
            }else{
                $event = new Comments();
                $event->event_id    = request('event_id');
                $event->user_id     = $user->id;
                $event->parent_id   = request('parent_id');
                $event->comment     = request('comment');
                $event->save();
                $eventId = $event->id;
                if($eventId){
                    
                    $message = 'Comment added successfully';
                    $status = 1;
                }else{
                    $message = 'Comment not added';
                    $status = 0;
                }
                return response()->json(['message'=>$message,'status'=>$status]);
            }
        }else{
            return response()->json(['message'=>'Invalid token','status'=>0,'data'=>array()]);
        }
    }
}
