<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Response;
class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $rules['email'] = 'required';
        $rules['password'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
                $user       = Auth::user();
                $token      = 'Bearer '.$user->createToken($user->id, [$user->user_role])->accessToken;
                $userData   = ['name'=>$user->name,'email'=>$user->email,'user_role'=>$user->user_role,'token'=>$token];
                return response()->json(['message'=>'Login successfully','status'=>1,'data'=>$userData]);
            }else{
                return response()->json(['message'=>'Invalid login credentials','status'=>0,'data'=>array()]);
            }
        }
    }
}
