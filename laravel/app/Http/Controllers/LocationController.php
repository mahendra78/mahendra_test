<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Models\Locations;
class LocationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if(@$user->id){
            $events = Locations::get();
            return response()->json(['message'=>'Locations List','status'=>1,'data'=>$events]);
        }else{
            return response()->json(['message'=>'Invalid token','status'=>0]);
        }
    }

    public function add(Request $request){
        $rules['location_name'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $location = new Locations();
            $location->location_name = request('location_name');
            $location->save();
            $locationId = $location->id;
            if($locationId){
                $message = 'Location added successfully';
                $status = 1;
            }else{
                $message = 'Location not added';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

    public function delete(Request $request){
        $rules['location_id'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $locationId = request('location_id');
            // Delete event
            $res = Locations::where('id',$locationId)->delete();
            if($res){
                $message = 'Location deleted successfully';
                $status = 1;
            }else{
                $message = 'Location not deleted';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

}
