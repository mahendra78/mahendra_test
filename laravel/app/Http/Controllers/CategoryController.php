<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Response;
use App\Models\Categories;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        if(@$user->id){
            $events = Categories::get();
            return response()->json(['message'=>'Categories List','status'=>1,'data'=>$events]);
        }else{
            return response()->json(['message'=>'Invalid token','status'=>0]);
        }
    }

    public function add(Request $request){
        $rules['category_name'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $category = new Categories();
            $category->name = request('category_name');
            $category->save();
            $categoryId = $category->id;
            if($categoryId){
                $message = 'Category added successfully';
                $status = 1;
            }else{
                $message = 'Category not added';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

    public function delete(Request $request){
        $rules['category_id'] = 'required';
        $messages = [
            'required' => 'Field is required'
        ];
        $validate = Validator::make($request->all(), $rules, $messages);
        if($validate->fails()) {
            $messages = $validate->messages();
            foreach ($messages->all(':message') as $message)
            {
                $m = $message;
            }
            return response()->json(['message'=>$m,'status'=>0,'data'=>array()]);
        }else{
            $categoryId = request('category_id');
            // Delete event
            $res = Categories::where('id',$categoryId)->delete();
            if($res){
                $message = 'Category deleted successfully';
                $status = 1;
            }else{
                $message = 'Category not deleted';
                $status = 0;
            }
            return response()->json(['message'=>$message,'status'=>$status]);
        }
    }

}
