<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;
use Response;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {

        if(Route::prefix('api')){
            echo json_encode(array('status'=>0,'message'=>'Invalid token'));
            die;
        }
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
