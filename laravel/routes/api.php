<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login','LoginController@index');

Route::get('envents','EventController@index');
Route::get('envents/{event_id}','EventController@enventDetails');
Route::post('envent/add','EventController@add')->middleware(['auth:api','scopes:admin']);
Route::post('envent/delete','EventController@delete')->middleware(['auth:api','scopes:admin']);
Route::post('envent/update','EventController@update')->middleware(['auth:api','scopes:admin']);

Route::get('locations','LocationController@index')->middleware(['auth:api','scopes:admin']);
Route::post('location/add','LocationController@add')->middleware(['auth:api','scopes:admin']);
Route::post('location/delete','LocationController@delete')->middleware(['auth:api','scopes:admin']);

Route::get('categories','CategoryController@index')->middleware(['auth:api','scopes:admin']);

Route::post('comment','EventController@addComment')->middleware(['auth:api','scope:admin,user']);
