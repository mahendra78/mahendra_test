# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Setup server ###

* Take clone inside your server root (like www or htdocs).
* You will find directory with name mahendra_test
* Use command "cd mahendra_test/laravel/"
* Run command composer install
* Find "mahendra_test.sql" file on root directory
* Chnage database connection details in .evn file
* Find postman export file "Test.postman_collection.json" and import in your postman. You can test all api request in post.


### React Setup ###

* Check your inside of mahendra_test directory
* Find react-test.zip file inside mahendra_test directory
* Unzip file react-test.zip
* Inside of mahendra_test directory run command "cd react-test"
* Run command "npm install"
* After successfully installation use command "npm run"
* Open "http://localhost:3000/" URL on browser


### User login details ###

* Admin login email : "admin@gmail.com" password : "12345"
* User login email : "mahendra@gmail.com" password : "12345"



